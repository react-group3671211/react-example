// 需要大写
const Test = () => {
    return <div>hello</div>;
};

const Header = () => {
    return <span>setting</span>;
};

const Footer = () => {
    return <div>footer</div>;
};

export default () => {
    return (
        <div>
            <Header />
            <Test />
            <Footer />
        </div>
    );
};
