import { useEffect, useState } from 'react';

import { useFetcher } from '@/components/fetcher/hooks';

export default () => {
    const fetcher = useFetcher();
    const [posts, setPosts] = useState([]);
    const [loading, setLoading] = useState(false);
    useEffect(() => {
        async function init() {
            try {
                const { data } = await fetcher.get('http://localhost:5173/api/data/list');
                console.log(data);
            } catch (err) {
                console.log(err);
            }
        }

        async function init_V2() {
            try {
                const response = await fetch('http://localhost:5173/api/data/list');
                const data = await response.json();
                console.log(data);
                setPosts(data.items);
            } catch (err) {
                console.log(err);
            }
        }

        init_V2();
    }, []);

    return (
        <>
            <div>monitor</div>
            <div>
                {loading &&
                    posts.map((post) => (
                        <div key={post.id}>
                            {post.name}
                            <img src={post.img} alt="good" />
                        </div>
                    ))}
            </div>
            <div>{loading ? <div>loading...</div> : <div>not loading</div>}</div>
            <div> {loading && <div>loading...</div>}</div>
            <button
                onClick={() => {
                    // eslint-disable-next-line @typescript-eslint/no-shadow
                    setLoading((loading) => !loading);
                }}
            >
                button
            </button>
        </>
    );
};
