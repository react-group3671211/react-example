import { ConfigProvider, theme } from 'antd';
import { StyleProvider } from '@ant-design/cssinjs';
import 'dayjs/locale/zh-cn';
import zhCN from 'antd/locale/zh_CN';

import { FC, ReactNode } from 'react';

import $styles from './App.module.css';

const App: FC = () => {
    return (
        <Container>
            <div className={$styles.container}>
                <h1>
                    欢迎来到3R教室，这是
                    <span className="tw-animate-decoration tw-cursor-pointer">React课程第一节</span>
                </h1>
            </div>
        </Container>
    );
};
const Container: FC<{ children?: ReactNode }> = ({ children }) => {
    return (
        <ConfigProvider
            locale={zhCN}
            theme={{
                algorithm: theme.defaultAlgorithm,
                token: {
                    colorPrimary: '#00B96B',
                },
                components: {
                    Layout: {
                        colorBgBody: '',
                    },
                },
            }}
        >
            <StyleProvider hashPriority="high">
                <div className={$styles.app}>{children}</div>
            </StyleProvider>
        </ConfigProvider>
    );
};

export default App;
