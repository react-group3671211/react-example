import { Button, ConfigProvider, theme } from 'antd';
import { StyleProvider } from '@ant-design/cssinjs';
import 'dayjs/locale/zh-cn';
import zhCN from 'antd/locale/zh_CN';

import clsx from 'clsx';

import { FC, ReactNode, useCallback, useEffect, useState } from 'react';

import $styles from './App.module.css';

const App: FC = () => {
    return (
        <Container>
            <div className={clsx($styles.container)}>
                <h1>
                    Welcome to theme project
                    <span className="tw-animate-decoration tw-cursor-pointer">React</span>
                </h1>
            </div>
            <div className={clsx($styles.test, 'tw-panel')}>测试</div>
        </Container>
    );
};
const Container: FC<{ children?: ReactNode }> = ({ children }) => {
    const [dark, setDark] = useState(true);
    const toggleDark = useCallback(() => setDark((dark) => !dark), []);
    useEffect(() => {
        const html = document.getElementsByTagName('html')[0];
        if (dark) {
            html.setAttribute('data-theme', 'tw-dark');
        } else {
            html.setAttribute('data-theme', 'tw-light');
        }
    }, [dark]);
    return (
        <ConfigProvider
            locale={zhCN}
            theme={{
                algorithm: theme.defaultAlgorithm,
                token: {
                    colorPrimary: '#00B96B',
                },
                components: {
                    Layout: {
                        colorBgBody: '',
                    },
                },
            }}
        >
            <StyleProvider hashPriority="high">
                <div className={clsx($styles.app)}>
                    <Button onClick={toggleDark}>切换暗黑模式</Button>
                    {children}
                </div>
            </StyleProvider>
        </ConfigProvider>
    );
};

export default App;
