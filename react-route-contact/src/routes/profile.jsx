export default function Profile() {
    return (
      <>
        <h1>Contacts</h1>
        <div className="arter">
          <p>
            欲穷千里目，更上一层楼。
          </p>
        </div>
        <h2>最喜欢的电视剧</h2>
        <ul>
          <li>潜伏</li>
          <li>士兵突击</li>
        </ul>
        <h2>最喜欢的运动</h2>
        <ol>
          <li>篮球</li>
          <li>跑步</li>
        </ol>
      </>
    );
  }
  