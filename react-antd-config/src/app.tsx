import { Button, ConfigProvider, theme } from 'antd';

import { StyleProvider } from '@ant-design/cssinjs';
import zhCN from 'antd/locale/zh_CN';

import $styles from './app.module.css';

const App = () => {
    return (
        <ConfigProvider
            locale={zhCN}
            theme={{
                algorithm: theme.defaultAlgorithm,
                token: {
                    colorPrimary: '#00B96B',
                },
                components: {
                    Layout: {
                        colorBgBody: '',
                    },
                },
            }}
        >
            <StyleProvider hashPriority="high">
                <div className={$styles.app}>
                    <div
                     className={$styles.container}>
                        welcome to <span>React课程第一节</span>
                    </div>
                    <div>
                    <Button
                            type="primary"
                            className="!tw-bg-lime-400 !tw-text-emerald-900"
                            href="https://www.google.com"
                            target="_blank"
                        >
                            点此打开
                        </Button>
                        <Button onClick={() => {alert("clicked");}}>
                            click
                        </Button>
                    </div>
                    <div className={$styles.content}>
床前明月光
                    </div>
                </div>
            </StyleProvider>
        </ConfigProvider>
    );
};

export default App;
