import {Outlet, Link, NavLink} from "react-router-dom";


function Layout () {
    return (
        <div>
            <h1>Layout</h1>
            <ul>
                <li><Link to="/test/app">app</Link></li>
                <li><Link to="/test/PersonList">PersonList</Link></li>
                <li><Link to="/test/MyComponent">MyComponent</Link></li>
                <li><Link to="/test/UseNavigation">UseNavigation</Link></li>
                <li><Link to="/test/RouteParam/8888">RouteParam</Link></li>
                <li><Link to="/test/SearchParam?id=2342332">SearchParam</Link></li>
                <li><NavLink to="/test/redirect">redirect</NavLink></li>
            </ul>
            {/*留给子组件Child的出口*/}
            <Outlet />
        </div>
    )
}

export default Layout
