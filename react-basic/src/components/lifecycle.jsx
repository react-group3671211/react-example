import React from 'react'

/**
 * 1. 初始化阶段: 由ReactDOM.render()触发---初次渲染
 *                                    1.    constructor()
 *                                    2.    componentWillMount()
 *                                    3.    render()
 *                                    4.    componentDidMount() =====> 常用
 * 一般在这个钩子中做一些初始化的事，例如：开启定时器、发送网络请求、订阅消息
 * 2. 更新阶段: 由组件内部this.setSate()或父组件render触发
 *                     1.    shouldComponentUpdate()
 *                     2.    componentWillUpdate()
 *                     3.    render() =====> 必须使用的一个
 *                     4.    componentDidUpdate()
 *  3. 卸载组件: 由ReactDOM.unmountComponentAtNode()触发
 *  1.    componentWillUnmount()  =====> 常用
 *  在这个钩子中做一些收尾的事，例如：关闭定时器、取消订阅消息
 */
class LifeCycleComponent extends React.Component {

    state = {opacity: 1}

    death = () => {
        console.log("pujie")
    }

    componentDidMount() {
        console.log('componentDidMount');
        this.timer = setInterval(() => {
            //获取原状态
            let {opacity} = this.state
            //减小0.1
            opacity -= 0.1
            if (opacity <= 0) opacity = 1
            //设置新的透明度
            this.setState({opacity})
        }, 200);
    }

    componentWillUnmount() {
        clearInterval(this.timer)
    }

    render() {
        console.log('render');
        return (
            <div>
                <h2 style={{opacity: this.state.opacity}}>偶夜色</h2>
                <button onClick={this.death}>BoyNextDoor</button>
            </div>
        )
    }

}

export default LifeCycleComponent
