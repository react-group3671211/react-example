import { useSearchParams } from "react-router-dom";

// 当前路径为 /foo?id=12
function SearchParam() {
    const [searchParams, setSearchParams] = useSearchParams();
    const id = searchParams.get("id");
    setSearchParams({
        name: "foo",
    }); // /foo?name=foo
    return <div>SearchParam : {id}</div>;
}

export default SearchParam
