import { useParams } from "react-router-dom";


export default function RouteParam() {
    const params = useParams();
    return (
        <div>
            <h1>{params.id}</h1>
        </div>
    );
}

