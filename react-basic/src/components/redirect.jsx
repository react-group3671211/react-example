import { Navigate } from "react-router-dom";

function Redirect() {
    return <Navigate to="/test" />;
}

export default Redirect
