import React from 'react'
import PropTypes from 'prop-types'

class MyComponent extends React.Component {

    constructor(props) {
        console.log('constructor');
        super(props)
        this.state = {isHot: false, wind: '微风', temp: "23.5"}

        //解决changeWeather中this指向问题
        this.changeWeather = this.changeWeather.bind(this)
    }


    /**
     * 直接在这里定义state也是可以的
     */
    state = {isHot: false, wind: '微风', temp: "23.5"}


    /**
     * 普通函数需要绑定this
     * 函数会在事件发生时被调用，但是函数内部的 this 并不会自动指向类实例。
     * 相反，它可能指向调用它的上下文，这会导致代码执行时出现问题。
     */
    changeWeather() {
        console.log('changeWeather');
        const isHot = this.state.isHot
        //严重注意：状态必须通过setState进行更新,且更新是一种合并，不是替换。
        this.setState({isHot:!isHot})
        console.log(this);
    }

    /**
     * 箭头函数不需要绑定this
     * 继承外层的this 外层一般是函数
     * 类也是一种函数 所以继承了外部Component的this
     */
    changeTemp = () => {
        console.log('changeTemp');
        const temp = this.state.temp
        this.setState({temp:temp+0.5})
        console.log(this);
    }


    showData = () => {
        const {you} = this.refs
        alert(you.value)
        alert(this.love.value)
    }

    /**
     * 通过event.target得到发生事件的DOM元素对象 ——————————不要过度使用ref
     * @returns {JSX.Element}
     */
    showDataV2 = (event) => {
        const {value} = event.target
        console.log(value)
    }

    //保存密码到状态中
    savePassword = (event)=>{
        this.setState({password:event.target.value})
    }

    render() {
        console.log('render中的this:', this);
        const {isHot, wind, temp} = this.state;
        const {userName,userEmail} = this.props;
        return (
            <>
                <h2>今天天气不错，继续加油 {userName} {userEmail}</h2>
                <p onClick={this.changeWeather}>今天天气很{isHot ? '炎热' : '凉爽'}，{wind}</p>
                <p onClick={this.changeTemp}>温度: {temp}</p>

                <input ref={"you"} type="text"/>
                <input ref={value => this.love = value}/>

                {/*受控input 状态保存到state中*/}
                <input onChange={this.savePassword}/>
                <button onClick={this.showData}></button>
            </>
        )
    }

}

MyComponent.propTypes = {
    userName:PropTypes.string.isRequired,
    userEmail:PropTypes.string,
    age:PropTypes.number,
}


MyComponent.defaultProps = {
    userName:'arterning',
    userEmail:'123@qq.com'
}

export default MyComponent
