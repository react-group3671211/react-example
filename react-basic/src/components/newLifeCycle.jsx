import React from 'react';
/*
1. 初始化阶段: 由ReactDOM.render()触发---初次渲染
                1.	constructor()
                2.	getDerivedStateFromProps
                3.	render()
                4.	componentDidMount() =====> 常用
                            一般在这个钩子中做一些初始化的事，例如：开启定时器、发送网络请求、订阅消息
2. 更新阶段: 由组件内部this.setSate()或父组件重新render触发
                1.	getDerivedStateFromProps
                2.	shouldComponentUpdate()
                3.	render()
                4.	getSnapshotBeforeUpdate
                5.	componentDidUpdate()
3. 卸载组件: 由ReactDOM.unmountComponentAtNode()触发
                1.	componentWillUnmount()  =====> 常用
                            一般在这个钩子中做一些收尾的事，例如：关闭定时器、取消订阅消息
    */
class Count extends React.Component{

    //构造器
    constructor(props){
        console.log('Count---constructor');
        super(props)
        this.state = {count:0}
    }

    add = ()=>{
        const {count} = this.state
        this.setState({count:count+1000})
    }

    death = ()=>{
        console.log("pujie")
    }

    //强制更新按钮的回调
    force = ()=>{
        this.forceUpdate()
    }

    //若state的值在任何时候都取决于props，那么可以使用getDerivedStateFromProps
    static getDerivedStateFromProps(props,state){
        console.log('getDerivedStateFromProps',props,state);
        return null
    }

    //在更新之前获取快照
    getSnapshotBeforeUpdate(){
        console.log('getSnapshotBeforeUpdate');
        return 'atguigu'
    }

    componentDidMount(){
        console.log('Count---componentDidMount');
    }

    componentWillUnmount(){
        console.log('Count---componentWillUnmount');
    }

    shouldComponentUpdate(){
        console.log('Count---shouldComponentUpdate');
        return true
    }

    componentDidUpdate(preProps,preState,snapshotValue){
        console.log('Count---componentDidUpdate',preProps,preState,snapshotValue);
    }

    render(){
        console.log('Count---render');
        const {count} = this.state
        return(
            <div>
                <h2>兄弟们的工资：{count}</h2>
                <button onClick={this.add}>点我+1</button>
                <button onClick={this.death}>卸载组件</button>
                <button onClick={this.force}>不更改任何状态中的数据，强制更新一下</button>
            </div>
        )
    }
}

export default Count
