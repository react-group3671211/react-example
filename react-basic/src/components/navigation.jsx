import { useNavigate } from 'react-router-dom';


function UseNavigation() {
    const navigate = useNavigate();

    return (
        <div>
            <h1>UseNavigation</h1>
            <div onClick={() => navigate('/test')}>跳转到test</div>
            <div onClick={() => navigate(-1)}>回退</div>
        </div>
    )
}

export default UseNavigation
