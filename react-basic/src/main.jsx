import React, {lazy, Suspense} from 'react'
import ReactDOM from 'react-dom/client'
import App from './App.jsx'
import './index.css'
import { BrowserRouter, Routes, Route } from "react-router-dom";
import PersonList from "./components/personList.jsx";
import MyComponent from "./components/class";
import Layout from "./components/layout.jsx";
import UseNavigation from "./components/navigation";
import Index from "./pages/index.jsx";
import RouteParam from "./components/routeParam.jsx";
import SearchParam from "./components/searchParam.jsx";
import Redirect from "./components/redirect.jsx";

const LazyComponent = lazy(()=> import("./components/lazy.jsx") )

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
      <BrowserRouter>
          <Routes>
              <Route path="/test" element={<Layout />}>
                  <Route index element={<Index />} />
                  <Route path="PersonList" element={<PersonList />} />
                  <Route path="MyComponent" element={<MyComponent/>}/>
                  <Route path="UseNavigation" element={<UseNavigation/>}/>
                  <Route path="RouteParam/:id" element={<RouteParam/>} />
                  <Route path="SearchParam" element={<SearchParam/>} />
                  <Route path="redirect" element={<Redirect />} />
                  <Route path="lazy" element={<LazyComponent />} />
                  <Route path="app" element={<App />} />
              </Route>
              <Route path="/" element={<App />} />
              <Route path="*" element={<h1>404</h1>} />
          </Routes>
      </BrowserRouter>
  </React.StrictMode>,
)
