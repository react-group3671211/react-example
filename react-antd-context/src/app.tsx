import ContextDemo, { Lang } from './components/demo/context';
import ReducerDemo, { Theme } from './components/demo/reducer';
import MasterLayout from './components/layout/master';

const App = () => {
    return (
        <Lang>
            <Theme>
                <MasterLayout>
                    {/* <StateDemo /> */}
                    {/* <EffectDemo /> */}
                    <ContextDemo />
                    <ReducerDemo />
                </MasterLayout>
            </Theme>
        </Lang>
    );
};

export default App;
