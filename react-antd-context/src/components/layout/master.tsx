import { FC, ReactNode, useContext, useEffect, useState } from 'react';
import { ConfigProvider, theme } from 'antd';

import { StyleProvider } from '@ant-design/cssinjs';

import { isNil } from 'lodash';

import { MappingAlgorithm } from 'antd/es/config-provider/context';

import { LangContext } from '../demo/context';

import { ThemeContext, defaultThemeConfig } from '../demo/reducer';

import $styles from './master.module.css';

const MasterLayout: FC<{ children?: ReactNode }> = ({ children }) => {
    const { lang } = useContext(LangContext);
    const themeContext = useContext(ThemeContext);
    const [antdThemes, setAntdThemes] = useState<MappingAlgorithm[]>([theme.defaultAlgorithm]);
    let themeMode = defaultThemeConfig.mode;
    let themeCompact = defaultThemeConfig.compact;
    if (!isNil(themeContext)) {
        themeMode = themeContext.state.mode;
        themeCompact = themeContext.state.compact;
    }
    useEffect(() => {
        if (!themeCompact) {
            setAntdThemes(() =>
                themeMode === 'dark' ? [theme.darkAlgorithm] : [theme.defaultAlgorithm],
            );
        } else {
            setAntdThemes(() =>
                themeMode === 'dark'
                    ? [theme.darkAlgorithm, theme.compactAlgorithm]
                    : [theme.defaultAlgorithm, theme.compactAlgorithm],
            );
        }
    }, [themeMode, themeCompact]);

    return (
        <ConfigProvider
            locale={lang.data}
            theme={{
                algorithm: antdThemes,
                components: {
                    Layout: {
                        colorBgBody: '',
                    },
                },
            }}
        >
            <StyleProvider hashPriority="high">
                <div className={$styles.masterLayout}>{children}</div>
            </StyleProvider>
        </ConfigProvider>
    );
};
export default MasterLayout;
