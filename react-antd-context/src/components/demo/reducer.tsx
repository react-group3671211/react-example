/* eslint-disable react/jsx-no-constructed-context-values */
import {
    Dispatch,
    FC,
    ReactNode,
    Reducer,
    createContext,
    useContext,
    useEffect,
    useReducer,
} from 'react';
import { produce } from 'immer';

import { isNil } from 'lodash';

import { Switch } from 'antd';

import $styles from './style.module.css';

export enum ThemeMode {
    LIGHT = 'light',
    DARK = 'dark',
}

export type ThemeState = {
    mode: `${ThemeMode}`;
    compact: boolean;
};

export enum ThemeActionType {
    CHANGE_MODE = 'change_mode',
    CHANGE_COMPACT = 'change_compact',
}

export type ThemeAction =
    | { type: `${ThemeActionType.CHANGE_MODE}`; value: `${ThemeMode}` }
    | { type: `${ThemeActionType.CHANGE_COMPACT}`; value: boolean };

export type ThemeContextType = {
    state: ThemeState;
    dispatch: Dispatch<ThemeAction>;
};

export const defaultThemeConfig: ThemeState = {
    mode: 'light',
    compact: false,
};

export const ThemeContext = createContext<ThemeContextType | null>(null);

export const ThemeReducer: Reducer<ThemeState, ThemeAction> = produce((draft, action) => {
    switch (action.type) {
        case 'change_mode':
            draft.mode = action.value;
            break;
        case 'change_compact':
            draft.compact = action.value;
            break;
        default:
            break;
    }
});

export const Theme: FC<{ data?: ThemeState; children?: ReactNode }> = ({ data = {}, children }) => {
    const [state, dispatch] = useReducer(ThemeReducer, data, (initData) => ({
        ...defaultThemeConfig,
        ...initData,
    }));
    useEffect(() => {
        if (state.mode === 'dark') {
            document.documentElement.setAttribute('data-theme', 'tw-dark');
        } else {
            document.documentElement.removeAttribute('data-theme');
        }
    }, [state.mode]);
    return <ThemeContext.Provider value={{ state, dispatch }}>{children}</ThemeContext.Provider>;
};

export const ThemeConfig: FC = () => {
    const context = useContext(ThemeContext);
    if (isNil(context)) return null;
    const { state, dispatch } = context;
    const toggleMode = () =>
        dispatch({ type: 'change_mode', value: state.mode === 'light' ? 'dark' : 'light' });
    const toggleCompact = () => dispatch({ type: 'change_compact', value: !state.compact });
    return (
        <>
            <Switch
                checkedChildren="🌛"
                unCheckedChildren="☀️"
                onChange={toggleMode}
                checked={state.mode === 'dark'}
                defaultChecked={state.mode === 'dark'}
            />
            <Switch
                checkedChildren="紧凑"
                unCheckedChildren="正常"
                onChange={toggleCompact}
                checked={state.compact}
                defaultChecked={state.compact}
            />
        </>
    );
};

const ReducerDemo: FC = () => {
    const context = useContext(ThemeContext);
    if (isNil(context)) return null;
    const {
        state: { mode, compact },
    } = context;
    return (
        <div className={$styles.container}>
            <h2 className="tw-text-center">useReducer Demo</h2>
            <p>主题模式: 「{mode === 'dark' ? '暗黑' : '明亮'}」</p>
            <p>是否紧凑: 「{compact ? '是' : '否'}」</p>
            <div className="tw-w-full">
                <ThemeConfig />
            </div>
        </div>
    );
};
export default ReducerDemo;
