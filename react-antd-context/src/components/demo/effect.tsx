import { FC, useEffect, useState } from 'react';

import { Button } from 'antd';

import $styles from './style.module.css';

const EffectDemo: FC = () => {
    const [ghost, setGhost] = useState<boolean>(false);
    const [red, setRed] = useState<boolean>(false);
    const [width, setWidth] = useState<number>(window.innerWidth);
    const resizeHandle = () => setWidth(window.innerWidth);
    useEffect(() => {
        window.addEventListener('resize', resizeHandle);
        return () => {
            window.removeEventListener('resize', resizeHandle);
        };
    }, []);
    useEffect(() => {
        (async () => {
            await new Promise((resolve, reject) => {
                setTimeout(() => resolve(true), 1000);
            });
            setRed(ghost);
        })();
    }, [ghost]);
    return (
        <div className={$styles.container}>
            <h2 className="tw-text-center">useEffect Demo</h2>
            <p className="tw-text-center tw-py-5">{ghost ? 'ghost' : '普通'}按钮</p>
            <Button type="primary" onClick={() => setGhost((v) => !v)} ghost={ghost} danger={red}>
                切换按钮样式
            </Button>
            <p className="tw-pt-5">宽度为: {width}</p>
        </div>
    );
};
export default EffectDemo;
