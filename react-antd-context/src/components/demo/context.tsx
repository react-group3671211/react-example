/* eslint-disable react/jsx-no-constructed-context-values */
import { FC, ReactNode, createContext, useContext, useState } from 'react';

import { Locale } from 'antd/es/locale';

import enUS from 'antd/es/locale/en_US';
import zhCN from 'antd/es/locale/zh_CN';

import { Pagination, Select } from 'antd';

import $styles from './style.module.css';

type LangType = {
    name: string;
    label: string;
    data: Locale;
};
type LangState = {
    lang: LangType;
    setLang: (lang: LangType) => void;
};
export const langs: LangType[] = [
    {
        name: 'en_US',
        label: '🇺🇸 english(US)',
        data: enUS,
    },
    {
        name: 'zh_CN',
        label: '🇨🇳 简体中文',
        data: zhCN,
    },
];
export const LangContext = createContext<LangState>({
    lang: langs[0],
    setLang: (lang: LangType) => {},
});
const LangProvider: FC<LangState & { children?: ReactNode }> = ({ lang, setLang, children }) => {
    return <LangContext.Provider value={{ lang, setLang }}>{children}</LangContext.Provider>;
};
export const Lang: FC<{ children?: ReactNode }> = ({ children }) => {
    const [lang, setLang] = useState<LangType>(langs[0]);
    return (
        <LangProvider lang={lang} setLang={setLang}>
            {children}
        </LangProvider>
    );
};
export const LangConfig: FC = () => {
    const { lang, setLang } = useContext(LangContext);
    const changeLang = (value: string) => {
        const current = langs.find((item) => item.name === value);
        current && setLang(current);
    };
    return (
        <Select defaultValue={lang.name} style={{ width: 120 }} onChange={changeLang}>
            {langs.map(({ name, label }) => (
                <Select.Option key={name} value={name}>
                    {label}
                </Select.Option>
            ))}
        </Select>
    );
};
const ContextDemo: FC = () => {
    const { lang } = useContext(LangContext);
    return (
        <div className={$styles.container}>
            <h2 className="tw-text-center">useContext Demo</h2>
            <p className="tw-text-center tw-py-5">当前语言: {lang.label}</p>
            <div className="tw-w-full">
                <h3>Antd语言切换测试</h3>
                <div className="tw-w-full tw-my-4">
                    <LangConfig />
                </div>
            </div>
            <Pagination defaultCurrent={0} total={500} />
        </div>
    );
};
export default ContextDemo;
