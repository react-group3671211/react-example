import { Button } from 'antd';
import { FC, useState } from 'react';
import clsx from 'clsx';

import $styles from './style.module.css';

const StateDemo: FC = () => {
    const [count, setCount] = useState<number>(1);
    const [show, setShow] = useState<boolean>(true);
    return (
        <div className={clsx($styles.container, 'tw-w-[20rem]')}>
            <h4 className="tw-text-center">useState Demo</h4>
            {show && <p className="tw-text-center tw-py-5">{count}</p>}
            <div className="tw-flex tw-justify-around">
                <Button onClick={() => setCount((v) => v + 1)} type="dashed">
                    增加
                </Button>
                <Button onClick={() => setCount((v) => v - 1)} type="dashed">
                    减少
                </Button>
                <Button onClick={() => setShow((v) => !v)} type="dashed">
                    {show ? '显示' : '隐藏'}
                </Button>
            </div>
        </div>
    );
};
export default StateDemo;
