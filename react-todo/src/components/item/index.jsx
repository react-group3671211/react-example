import  { Component } from 'react'
import './index.css'
import PropTypes from "prop-types";

export default class Item extends Component {

    state = {mouse:false} //标识鼠标移入、移出

    static propTypes = {
        updateTodo:PropTypes.func.isRequired,
        deleteTodo:PropTypes.func.isRequired,
        id: PropTypes.number,
        name: PropTypes.string,
        done: PropTypes.bool
    }


    //鼠标移入、移出的回调
    handleMouse = (flag)=>{
        return ()=>{
            this.setState({mouse:flag})
        }
    }

    //勾选、取消勾选某一个todo的回调
    handleCheck = (id)=>{
        return (event)=>{
            this.props.updateTodo(id,event.target.checked)
        }
    }

    //删除一个todo的回调
    handleDelete = (id)=>{
        if(window.confirm('确定删除吗？')){
            this.props.deleteTodo(id)
        }
    }


    render() {
        const {id,name,done} = this.props
        const {mouse} = this.state
        return (
            <li style={{backgroundColor:mouse ? '#ddd' : 'white'}} onMouseEnter={this.handleMouse(true)} onMouseLeave={this.handleMouse(false)}>
                <label>
                    <input type="checkbox" checked={done} onChange={this.handleCheck(id)}/>
                    <span>{name}</span>
                </label>
                <button onClick={()=> this.handleDelete(id) } className="btn btn-danger" style={{display:mouse?'block':'none'}}>删除</button>
            </li>
        )
    }
}
